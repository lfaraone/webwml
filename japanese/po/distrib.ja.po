msgid ""
msgstr ""
"Project-Id-Version: distrib.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-04-24 17:00+0900\n"
"Last-Translator: Nobuhiro Iwamatsu <iwamatsu@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/distrib/search_packages-form.inc:8
#: ../../english/distrib/search_contents-form.inc:9
msgid "Keyword"
msgstr "キーワード"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "検索対象"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "パッケージ名のみ"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "パッケージ説明文"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "ソースパッケージ名"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "完全にマッチするもののみを表示"

#: ../../english/distrib/search_packages-form.inc:25
#: ../../english/distrib/search_contents-form.inc:25
msgid "Distribution"
msgstr "ディストリビューション"

#: ../../english/distrib/search_packages-form.inc:27
#: ../../english/distrib/search_contents-form.inc:27
msgid "experimental"
msgstr "experimental"

#: ../../english/distrib/search_packages-form.inc:28
#: ../../english/distrib/search_contents-form.inc:28
msgid "unstable"
msgstr "不安定版 (unstable)"

#: ../../english/distrib/search_packages-form.inc:29
#: ../../english/distrib/search_contents-form.inc:29
msgid "testing"
msgstr "テスト版 (testing)"

#: ../../english/distrib/search_packages-form.inc:30
#: ../../english/distrib/search_contents-form.inc:30
msgid "stable"
msgstr "安定版 (stable)"

#: ../../english/distrib/search_packages-form.inc:31
#: ../../english/distrib/search_contents-form.inc:31
msgid "oldstable"
msgstr "旧安定版 (oldstable)"

#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
#: ../../english/distrib/search_contents-form.inc:38
msgid "any"
msgstr "すべて"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "セクション"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/distrib/search_packages-form.inc:43
#: ../../english/distrib/search_contents-form.inc:48
msgid "Search"
msgstr "検索"

#: ../../english/distrib/search_packages-form.inc:44
#: ../../english/distrib/search_contents-form.inc:49
msgid "Reset"
msgstr "リセット"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "結果表示方法"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "キーワードを末尾に持つパス"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "キーワードに似た名前のファイルを含むパッケージ"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "キーワードを含む名前のファイルを含むパッケージ"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "アーキテクチャ"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64 ビット PC (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "64 ビット PC (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "Hard Float ABI ARM (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd 32 ビット PC (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "32 ビット PC (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD 32 ビット PC (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD 64 ビット PC (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (ビッグエンディアン)"

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr "64 ビット MIPS (リトルエンディアン)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (リトルエンディアン)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr "POWER プロセッサ"

#: ../../english/releases/arches.data:26
#, fuzzy
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "64 ビット MIPS (リトルエンディアン)"

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr "SPARC"

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that the function ST_AsX3D in PostGIS, a module that
adds spatial objects to the PostgreSQL object-relational database, did
not handle empty values properly, allowing malicious users to cause
denial of service or possibly other unspecified behaviour.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.1.4+dfsg-3+deb8u1.</p>

<p>We recommend that you upgrade your postgis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1653.data"
# $Id: $

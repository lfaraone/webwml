<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that partclone, an utility to backup partitions,
was prone to a heap-based buffer overflow vulnerability due to
insufficient validation of the partclone image header. This could allow
remote attackers to cause a <q>Denial of Service attack</q> in the context
of the user running the affected application via a crafted partition
image.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.2.48-1+deb7u1.</p>

<p>We recommend that you upgrade your partclone packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-923.data"
# $Id: $

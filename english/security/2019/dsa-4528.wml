<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Daniel McCarney discovered that the BIRD internet routing daemon
incorrectly validated RFC 8203 messages in it's BGP daemon, resulting
in a stack buffer overflow.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.6.6-1+deb10u1. In addition this update fixes an incomplete
revocation of privileges and a crash triggerable via the CLI (the latter
two bugs are also fixed in the oldstable distribution (stretch) which is
not affected by
<a href="https://security-tracker.debian.org/tracker/CVE-2019-16159">\
CVE-2019-16159</a>).</p>

<p>We recommend that you upgrade your bird packages.</p>

<p>For the detailed security status of bird please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bird">\
https://security-tracker.debian.org/tracker/bird</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4528.data"
# $Id: $

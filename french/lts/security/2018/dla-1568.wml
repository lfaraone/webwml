#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans cURL, une bibliothèque de
transfert d’URL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7141">CVE-2016-7141</a>

<p>Lorsque construite avec NSS et que la bibliothèque libnsspem.so est
disponible au moment de l’exécution, cela permet à un attaquant distant de détourner
l’authentification d’une connexion TLS en exploitant la réutilisation d’un
certificat de client précédemment chargé à partir d’un fichier pour une
connexion n’ayant aucun certificat de défini, une vulnérabilité différente
de <a href="https://security-tracker.debian.org/tracker/CVE-2016-5420">CVE-2016-5420</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7167">CVE-2016-7167</a>

<p>Plusieurs dépassement d'entiers dans les fonctions (1) curl_escape, (2)
curl_easy_escape, (3) curl_unescape et (4) curl_easy_unescape dans libcurl
permettent à des attaquants d’avoir un impact non précisé à l'aide d'un chaîne
de longueur 0xffffffff, déclenchant un dépassement de tampon basé sur le tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9586">CVE-2016-9586</a>

<p>Curl est vulnérable à un dépassement de tampon lorsque produisant une grande
sortie à virgule flottante dans l’implémentation des fonctions
printf() de libcurl. S’il existe une application qui accepte une chaîne de formatage
de l’extérieur sans filtrage nécessaire d’entrée, cela pourrait permettre des
attaques distantes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16839">CVE-2018-16839</a>

<p>Curl est vulnérable à un dépassement de tampon dans le code d'authentification
SASL qui pourrait conduire à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16842">CVE-2018-16842</a>

<p>Curl est vulnérable à une lecture hors limites de tampon basé sur le tas
dans la fonction tool_msgs.c:voutf() qui pourrait aboutir à une exposition
d’informations et un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 7.38.0-4+deb8u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1568.data"
# $Id: $

#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Kerberos, un système pour authentifier les utilisateurs et les services
sur un réseau, était affecté par plusieurs vulnérabilités. Le projet
« Common Vulnerabilities and Exposures » (CVE) identifie les problèmes
suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-1418">CVE-2013-1418</a>

<p>Kerberos permet à des attaquants distants de provoquer un déni de
service (déréférencement de pointeur NULL et plantage du démon) à l'aide
d'une requête contrefaite lorsque de multiples domaines sont configurés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-5351">CVE-2014-5351</a>

<p>Kerberos envoie d'anciennes clés dans une réponse à une requête -randkey
-keepold. Cela permet à des utilisateurs distants authentifiés de
contrefaire des tickets en tirant parti d'un accès d'administration.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-5353">CVE-2014-5353</a>

<p>Quand KDC utilise LDAP, il permet à des utilisateurs distants
authentifiés de provoquer un déni de service (plantage du démon) à l'aide
d'une requête LDAP réussie sans résultat, comme cela a été démontré en
utilisant un type d'objet incorrect pour une politique de mot de passe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-5355">CVE-2014-5355</a>

<p>Kerberos s'attend à ce qu'un champ de données krb5_read_message soit
représenté par une chaîne finissant par un caractère « 0 ». Cela permet à
des attaquants distants (1) de provoquer un déni de service
(déréférencement de pointeur NULL) à l'aide d'une chaîne de version zéro
octet ou (2) de provoquer un déni de service (lecture hors limites) en
omettant le caractère « 0 ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3119">CVE-2016-3119</a>

<p>Kerberos permet à des utilisateurs distants authentifiés de provoquer un
déni de service (déréférencement de pointeur NULL et plantage du démon) à
l'aide d'une requête contrefaite pour modifier un nom principal.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3120">CVE-2016-3120</a>

<p>Kerberos permet à des utilisateurs distants authentifiés de provoquer un
déni de service (déréférencement de pointeur NULL et plantage du démon) à
l'aide d'une requête S4U2Self.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.10.1+dfsg-5+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets krb5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1265.data"
# $Id: $

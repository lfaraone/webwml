#use wml::debian::translation-check translation="5cce6e19042ece6cac569ce8e96257a21f73442b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la servlet
Tomcat et le moteur JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17569">CVE-2019-17569</a>

<p>La réécriture dans 7.0.98 introduisait une régression. Le résultat de la
régression faisait que les en-têtes Transfer-Encoding non valables étaient
traités incorrectement conduisant à une possibilité de dissimulation de requête
HTTP si Tomcat résidait derrière un mandataire inverse qui gérait
incorrectement l’en-tête Transfer-Encoding non valable d’une certaine manière.
Un tel mandataire inverse est considéré comme peu probable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1935">CVE-2020-1935</a>

<p>Le code d’analyse d’en-têtes HTTP utilisait une manière d’analyse de fin
de ligne (EOL) qui permettait à quelques en-têtes HTTP non valables d’être
analysés comme valables. Cela conduisait à une possibilité de dissimulation de
requête HTTP si Tomcat résidait derrière un mandataire inverse qui gérait
incorrectement l’en-tête Transfer-Encoding non valable d’une certaine manière.
Un tel mandataire inverse est considéré comme peu probable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">CVE-2020-1938</a>

<p>Lors de l’utilisation du protocole Apache JServ (AJP), une attention doit
être portée lors de la confiance dans les connexions entrantes vers Apache
Tomcat. Tomcat traite les connexions AJP comme de plus haute confiance que,
par exemple, une connexion HTTP similaire. Si de telles connexions sont
disponibles à un attaquant, elles peuvent être exploitées de manières
surprenantes. Avant sa version 7.0.100, Tomcat était fourni avec un connecteur
AJP activé par défaut qui écoutait toutes les adresses IP configurées. Il était
prévu (et recommandé dans le guide de sécurité) que ce connecteur soit désactivé
s’il n’était pas requis.</p>

<p>Il est à remarquer que Debian désactive le connecteur  AJP par défaut. La
mitigation est seulement nécessaire si le port AJP était accessible aux
utilisateurs non fiables.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 7.0.56-3+really7.0.100-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2133.data"
# $Id: $
